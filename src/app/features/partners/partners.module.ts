import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../shared/shared.module";
import {ListComponent} from './list/list.component';
import {EditComponent} from './edit/edit.component';
import {DetailComponent} from './detail/detail.component';
import {AuthGuard} from "../../core/guards/auth.guard";
import {NzWaveModule} from "ng-zorro-antd/core/wave";
import {NzButtonModule} from "ng-zorro-antd/button";
import {NzIconModule} from "ng-zorro-antd/icon";
import {NzTypographyModule} from "ng-zorro-antd/typography";
import {NzCardModule} from "ng-zorro-antd/card";
import {NzGridModule} from "ng-zorro-antd/grid";
import {NewComponent} from './new/new.component';
import {NzPageHeaderModule} from "ng-zorro-antd/page-header";

const routes: Routes = [
  {path: 'partners/list', component: ListComponent, canActivate: [AuthGuard]},
  {path: 'partners/new', component: NewComponent, canActivate: [AuthGuard]},
  {path: 'partners/:uuid/detail', component: DetailComponent, canActivate: [AuthGuard]},
  {path: 'partners/:uuid/edit', component: EditComponent, canActivate: [AuthGuard]},
];

@NgModule({
  declarations: [
    ListComponent,
    EditComponent,
    DetailComponent,
    NewComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes),
    SharedModule,
    NzWaveModule,
    NzButtonModule,
    NzIconModule,
    NzTypographyModule,
    NzCardModule,
    NzGridModule,
    NzPageHeaderModule
  ]
})
export class PartnersModule {
}
