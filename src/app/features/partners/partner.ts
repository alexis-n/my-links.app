export interface Partner {
  id: number;
  uuid: string;
  id_company: number;
  name: string;
  address: string;
  additional_address: string;
  city: string;
  zipcode: number;
  contact: string;
  phone: number;
  mobile: number;
  email: string;
  website: string;
  image: string;
  created_at: string;
  updated_at: string;
  deleted_at: string;
}
