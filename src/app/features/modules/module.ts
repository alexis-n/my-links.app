export interface Module {
  id: number;
  name: string;
  description: string;
  icon: string;
  created_at: string;
  updated_at: string;
  deleted_at: string;
}
