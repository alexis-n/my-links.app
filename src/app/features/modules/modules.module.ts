import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ModulesComponent} from './modules.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../shared/shared.module";
import {AuthGuard} from "../../core/guards/auth.guard";

const routes: Routes = [
  {path: 'modules', component: ModulesComponent, canActivate: [AuthGuard]},
];

@NgModule({
  declarations: [
    ModulesComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes),
    SharedModule
  ]
})
export class ModulesModule {
}
