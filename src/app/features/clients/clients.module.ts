import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SharedModule} from '../../shared/shared.module';
import {RouterModule, Routes} from '@angular/router';
import {DetailComponent} from './detail/detail.component';
import {ListComponent} from './list/list.component';
import {EditComponent} from './edit/edit.component';
import {AuthGuard} from '../../core/guards/auth.guard';

const routes: Routes = [
  {path: 'clients/list', component: ListComponent, canActivate: [AuthGuard]},
  {path: 'clients/:uuid/detail', component: DetailComponent, canActivate: [AuthGuard]},
  {path: 'clients/:uuid/edit', component: EditComponent, canActivate: [AuthGuard]},
];

@NgModule({
  declarations: [
    ListComponent,
    DetailComponent,
    EditComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes),
    SharedModule
  ]
})
export class ClientsModule {
}
