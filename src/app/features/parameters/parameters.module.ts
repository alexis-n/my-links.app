import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from "@angular/router";
import {ParametersComponent} from "./parameters.component";
import {AuthGuard} from "../../core/guards/auth.guard";
import {SharedModule} from "../../shared/shared.module";

const routes: Routes = [
  {path: 'parameters', component: ParametersComponent, canActivate: [AuthGuard]},
];

@NgModule({
  declarations: [
    ParametersComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes),
    SharedModule
  ]
})
export class ParametersModule {
}
