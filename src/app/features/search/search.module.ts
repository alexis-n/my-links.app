import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from "@angular/router";
import {SearchComponent} from "./search.component";
import {AuthGuard} from "../../core/guards/auth.guard";
import {SharedModule} from "../../shared/shared.module";

const routes: Routes = [
  {path: 'search', component: SearchComponent, canActivate: [AuthGuard]},
];

@NgModule({
  declarations: [
    SearchComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes),
    SharedModule
  ]
})
export class SearchModule {
}
