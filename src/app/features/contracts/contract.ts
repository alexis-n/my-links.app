export interface Contract {
  id: number;
  uuid: number;
  number: string;
  title: string;
  id_client: number;
  id_product: number;
  begin_date: string;
  end_date: string;
  uniq_bonus: number;
  annual_bonus: number;
  created_at: string;
  updated_at: string;
  deleted_at: string;
}
