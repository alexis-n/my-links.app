import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "../../shared/shared.module";
import {EditComponent} from './edit/edit.component';
import {DetailComponent} from './detail/detail.component';
import {ListComponent} from './list/list.component';
import {AuthGuard} from "../../core/guards/auth.guard";

const routes: Routes = [
  {path: 'contracts/list', component: ListComponent, canActivate: [AuthGuard]},
  {path: 'contracts/:uuid/detail', component: DetailComponent, canActivate: [AuthGuard]},
  {path: 'contracts/:uuid/edit', component: EditComponent, canActivate: [AuthGuard]},
];

@NgModule({
  declarations: [
    EditComponent,
    DetailComponent,
    ListComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes),
    SharedModule
  ]
})
export class ContractsModule {
}
