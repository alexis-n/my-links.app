import {NgModule} from '@angular/core';
import {CoreModule} from './core/core.module';
import {AppComponent} from './app.component';
import {AppRoutingModule} from './app-routing.module';
import {DashboardModule} from './features/dashboard/dashboard.module';
import {ClientsModule} from './features/clients/clients.module';
import {SearchModule} from "./features/search/search.module";
import {PartnersModule} from "./features/partners/partners.module";
import {ContractsModule} from "./features/contracts/contracts.module";
import {ModulesModule} from "./features/modules/modules.module";
import {SharedModule} from "./shared/shared.module";
import {ProfileModule} from "./features/profile/profile.module";
import {ParametersModule} from "./features/parameters/parameters.module";

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    CoreModule,
    DashboardModule,
    SearchModule,
    ModulesModule,
    ContractsModule,
    ClientsModule,
    PartnersModule,
    ProfileModule,
    ParametersModule,
    AppRoutingModule,
    SharedModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
