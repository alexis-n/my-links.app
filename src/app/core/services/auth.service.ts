import {Injectable} from '@angular/core';
import {AngularFireAuth} from '@angular/fire/compat/auth';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private angularFireAuth: AngularFireAuth
  ) {
    this.angularFireAuth.authState.subscribe((user) => {
      localStorage.setItem('user', JSON.stringify(user));
      JSON.parse(localStorage.getItem('user')!);
    });
  }

  //
  get isLoggedIn(): boolean {
    const user = JSON.parse(localStorage.getItem('user')!);
    return (user !== null && user.emailVerified !== false);
  }

  //
  signIn(email: string, password: string) {
    return this.angularFireAuth.signInWithEmailAndPassword(email, password);
  }

  //
  signUp(email: string, password: string) {
    return this.angularFireAuth.createUserWithEmailAndPassword(email, password);
  }

  //
  signOut() {
    return this.angularFireAuth.signOut();
  }

  //
  forgot(email: string) {
    return this.angularFireAuth.sendPasswordResetEmail(email);
  }

  //
  reset() {
    return this.angularFireAuth.currentUser;
  }

}
