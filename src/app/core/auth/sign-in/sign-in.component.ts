import {Component, NgZone} from '@angular/core';
import {AuthService} from '../../services/auth.service';
import {Router} from "@angular/router";
import firebaseErrors from '../firebaseErrors.json';
import {FormControl, FormGroup} from "@angular/forms";

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['../auth.component.css']
})
export class SignInComponent {

  signInForm = new FormGroup({
    email: new FormControl(''),
    password: new FormControl('')
  });

  errorMessage?: string;

  constructor(
    private authService: AuthService,
    private ngZone: NgZone,
    private router: Router
  ) {
  }

  get email() {
    return this.signInForm.get('email')?.value;
  }

  get password() {
    return this.signInForm.get('password')?.value;
  }

  //
  signIn() {
    this.authService.signIn(this.email, this.password).then(
      () => {
        this.ngZone.run(() => {
          this.router.navigate(['dashboard']);
        });
      }
    ).catch(
      (error) => {
        this.errorMessage = firebaseErrors.find(e => e.code === error.code)?.message;
      }
    )
  }

}
