import {Component, NgZone} from '@angular/core';
import {AuthService} from '../../services/auth.service';
import firebaseErrors from "../firebaseErrors.json";
import {FormControl, FormGroup} from "@angular/forms";
import {Router} from "@angular/router";

@Component({
  selector: 'app-forgot',
  templateUrl: './forgot.component.html',
  styleUrls: ['../auth.component.css']
})
export class ForgotComponent {

  forgotForm = new FormGroup({
    email: new FormControl('')
  });

  errorMessage?: string;
  successMessage?: string;

  constructor(
    private authService: AuthService,
    private ngZone: NgZone,
    private router: Router
  ) {
  }

  get email() {
    return this.forgotForm.get('email')?.value;
  }

  forgot() {
    this.authService.forgot(this.email).then(
      () => {
        this.ngZone.run(() => {
          this.router.navigate(['reset']);
        });
      }).catch((error) => {
      this.errorMessage = firebaseErrors.find(e => e.code === error.code)?.message;
    });
  }

}
