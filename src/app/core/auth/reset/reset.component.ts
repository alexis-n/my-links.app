import {Component} from '@angular/core';
import {AuthService} from '../../services/auth.service';
import {Router} from "@angular/router";
import firebaseErrors from "../firebaseErrors.json";

@Component({
  selector: 'app-reset',
  templateUrl: './reset.component.html',
  styleUrls: ['../auth.component.css']
})
export class ResetComponent {

  errorMessage?: string;

  constructor(
    private authService: AuthService,
    public router: Router
  ) {
  }

  //
  reset(): void {
    this.authService.reset().then(
      (user) => user?.sendEmailVerification().then(
        () => {
          this.router.navigate(['reset'])
        }).catch((error) => {
        this.errorMessage = firebaseErrors.find(e => e.code === error.code)?.message;
      })
    );
  }

}
