import {Component} from '@angular/core';
import {AuthService} from '../../services/auth.service';
import firebaseErrors from "../firebaseErrors.json";
import {FormControl, FormGroup} from "@angular/forms";

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['../auth.component.css']
})
export class SignUpComponent {

  signUpForm = new FormGroup({
    email: new FormControl(''),
    password: new FormControl('')
  });

  errorMessage?: string;

  constructor(
    private authService: AuthService
  ) {
  }

  get email() {
    return this.signUpForm.get('email')?.value;
  }

  get password() {
    return this.signUpForm.get('password')?.value;
  }

  signUp() {
    this.authService.signUp(this.email, this.password).then(
      () => {}
    ).catch(
      (error) => {
        this.errorMessage = firebaseErrors.find(e => e.code === error.code)?.message;
      }
    )
  }

}
