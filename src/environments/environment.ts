// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyANpxu6i27KD0NEb8gocKBQHXr7VZ3E_U8",
    authDomain: "mylinks-a460a.firebaseapp.com",
    projectId: "mylinks-a460a",
    storageBucket: "mylinks-a460a.appspot.com",
    messagingSenderId: "786662670306",
    appId: "1:786662670306:web:28cdfee782f35d2ec103bb",
    measurementId: "G-D706XKHM8H"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
